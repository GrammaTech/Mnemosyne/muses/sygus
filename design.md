Design for SyGuS muse

Please indicate what of the following is erroneous, and fill
in additional details you think I should know about.  For example,
point out specific examples where these are done in other muses.

SyGuS clients are programs that take as input an SMTLIB2 style
syntax (much like S expressions) representing constraints and
produce as output more S expressions showing a synthesized program.

A muse for SyGuS must do the following:

  Have a representation of the code being manipulated.

  Receive codeAction messages from Mnemosyne, indicating
  a segment of the source code.

  Invoke a specified SyGuS program on this information
  and either update the program (and inform Mnemosyne of the
  update with a code-action message) or fail and inform Mnemosyne
  with a ??? message.

  If a SyGuS server is like an SMT server, then it could instead
  be persistent.  However, if there is no need for it to retain
  state it could be started fresh each time.

Conversion of code-action messages to SyGuS invocations
involves the following:
   
  Extraction of the segment of code representing the
  the conditions to be passed to the program.

  Convert this segment to the input form form expected
  by the program.   The SyGuS syntax is cvery close to SMTLIB2,
  which is lisp-like.  It can be written and read using
  the Lisp printer and reader, with minor tweaks.

  Add other information to be sent to the program,
  including command line options, the theory to be
  synthesized under, and time limits.

  Invoke the program.

  If the program aborts or times out, send a failure message
  (what is this?) back to Mnemosyne.

  If the program succeeds, convert the SyGuS output to
  Python using lenses.  If the lenses can work on s-exprs
  do that directly, otherwise convert them to some AST
  form.

  Create a documentEdit message and send that to the
  argot server.  I understand this is also forwarded
  to the LSP client.

The SyGuS muse will be implemented in Common Lisp, using the
argot-server infrastructure.

  I want to run this inside the argot-server just for simplicity,
  at least for debugging, if that's not a problem.  The design
  should be such that it can be split off to a separate server
  process.  (Which socket # would this use?)

Comments from Paul R.:

Which existing muse is most similar depends on a number of questions:
do the programs take input via stdio or via TCP? If stdio,
interactively or in batches?

That said, I would expect that Trinity is probably the most similar
existing muse (since it’s already doing synthesis). Trinity uses
two-round code actions, which are asynchronous; assuming SyGuS is
relatively slow (>1 minute?) it should do the same or it will time
out. (Timeouts are not actually in place yet, but should be soon.)

   There are many SyGuS clients.  I do expect some (most?) will be slow.

There’s no mention of a step that converts the code to
s-expressions. Would this also use lenses?

   There are two things here.  First, what does the specification
   look like in our code?  I was imagining it was in S-exprs in comments,
   but should it look like something else?  If so, it also needs to
   be translated

There are a couple of existing Lisp libraries work for working with
SMT and SAT, under the github.com/cl-model-languages
organization. CL-SMT in particular already has code for bridging
SMTLIB2 and CL S-expressions.

   Good to know.  I've implemented this sort of thing before, but
   it's better to use some preexisting scheme.  I will focus on CL-SMT.
   The hardest part has been model extraction from the SMT solver.
   I don't think that will be a problem here.

Extraction of the code segment should already be handled by the Argot
Server infrastructure.

  (Note to self: look up how it does this and record here.)

If you need to have the user provide options that can be done now
using text prompts, but (for now) only in VS Code.

  (Note: look up how this is done and record here.)

Running inside Argot Server is fine. If it has to be split into a
separate server, that server would find out what port to listen to by
checking the PORT environment variable when it starts up (Argot Server
binds it to a free port). That said it should still have a default
port. 4000, 4500, and 5000 are taken. To continue the pattern, perhaps
4250 or 4750?

  (Note: specify default port somewhere.  Should the argot-server
  infrastructure have a way of registering theses?)
  
